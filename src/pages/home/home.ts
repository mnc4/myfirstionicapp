import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { EsempioPage } from '../esempio/esempio';

import { HTTP } from '@ionic-native/http';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	my_name = "";
	char_number = 0;
	lista = [];

	constructor(public navCtrl: NavController, private http: HTTP, private streamingMedia: StreamingMedia) {
		console.log("COSTRUTTO PARTITO");

	}

	ngOnInit() {
		console.log("INIIT PARTITO");
	}
	
	text_change() {
		this.char_number=this.my_name.length;
	}

	button_clicked() {
		this.navCtrl.push(EsempioPage,{nome_oggetto: this.my_name});
		/*this.lista.push(
			{
				nome: this.my_name,
				numero_caratteri: this.char_number
			}
		);
		this.my_name="";
		console.log(this.lista);*/
		/*this.http.get('http://ionic.io', {}, {})
		.then(data => {
			console.log("SUCCESS");
			console.log(data.status);
			console.log(data.data); // data received by server
			console.log(data.headers);

		})
		.catch(error => {
			console.log("ERROR");
			console.log(error.status);
			console.log(error.error); // error message as string
			console.log(error.headers);

		});*/

		/*let options: StreamingVideoOptions = {
		  successCallback: () => { console.log('Video played') },
		  errorCallback: (e) => { console.log('Error streaming') },
		  orientation: 'landscape',
		  shouldAutoClose: true,
		  controls: true
		};
		this.streamingMedia.playVideo('http://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_480_1_5MG.mp4', options);*/
	}
}
