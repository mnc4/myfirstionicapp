import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EsempioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-esempio',
  templateUrl: 'esempio.html',
})
export class EsempioPage {

	nome_oggetto="";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EsempioPage');
  }

  ngOnInit() {
		this.nome_oggetto = this.navParams.get('nome_oggetto');
	}

}


