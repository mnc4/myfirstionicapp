import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EsempioPage } from './esempio';

@NgModule({
  declarations: [
    EsempioPage,
  ],
  imports: [
    IonicPageModule.forChild(EsempioPage),
  ],
})
export class EsempioPageModule {}
